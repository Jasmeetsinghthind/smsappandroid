package com.example.thind.sms.Classes;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.thind.sms.Adapter.Adapter;
import com.example.thind.sms.Comparator.ById;
import com.example.thind.sms.Comparator.ByName;
import com.example.thind.sms.Database.DatabaseController;
import com.example.thind.sms.R;
import com.example.thind.sms.util.Conventions;
import com.example.thind.sms.util.DataRecieveInterface;
import com.example.thind.sms.util.DialogListener;

import java.util.Collections;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements DialogListener, Conventions, DataRecieveInterface {
    Adapter adapter;
    Intent intentObj;
    LinkedList<Student> studentData;
    protected int pc;
    protected Button list, grid;
    protected ListView listView;
    protected GridView gridView;
    Student StdData;
    SharedPreferences preference;

    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String mActivityTitle;
    AddStudentForm addform;
    FragmentManager fragmentManager;
    RelativeLayout parent, fr1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        studentData = new LinkedList<>();
        list = (Button) findViewById(R.id.list);
        grid = (Button) findViewById(R.id.grid);
        //  addDetailsButton = (Button) findViewById(R.id.addButton);
        // logout = (Button) findViewById(R.id.logout);
        listView = (ListView) findViewById(R.id.viewList);
        gridView = (GridView) findViewById(R.id.viewgrid);
        mDrawerList = (ListView) findViewById(R.id.navList);
        parent = (RelativeLayout) findViewById(R.id.parent);
        fr1 = (RelativeLayout) findViewById(R.id.fr1);
        adapter = new Adapter(studentData, this, R.layout.activity_after_save);
        preference = PreferenceManager.getDefaultSharedPreferences(this);
        listView.setAdapter(adapter);
        new Downloader().execute(VIEW_ALL_ASYN);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pc = position;
                view();
            }
        });
        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.setTextColor(Color.rgb(160, 160, 160));
                grid.setTextColor(Color.rgb(0, 128, 255));
                listView.setVisibility(v.INVISIBLE);
                gridView.setVisibility(v.VISIBLE);
                gridView.setAdapter(adapter);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        pc = position;
                        view();
                    }
                });
            }
        });
        list.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                list.setTextColor(Color.rgb(0, 128, 255));
                grid.setTextColor(Color.rgb(160, 160, 160));
                gridView.setVisibility(v.INVISIBLE);
                listView.setVisibility(v.VISIBLE);
                listView.setAdapter(adapter);
            }
        });
//        addDetailsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//              //  addDetails();
//                parent.setVisibility(v.INVISIBLE);
////                FragmentManager FM=getFragmentManager();
////                FragmentTransaction FT=FM.beginTransaction();
////                addform= new AddStudentForm();
////                FT.add(R.id.fr1,addform,"add_fragment");
////                FT.commit();
//                callFragment();
//            }
//        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                logout();
//            }
//        });
        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String sortBy = spinner.getSelectedItem().toString();
                if (sortBy.equals("Roll_No")) {
                    Collections.sort(studentData, new ById());
                    adapter.notifyDataSetChanged();
                }
                if (sortBy.equals("Name")) {
                    Collections.sort(studentData, new ByName());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        addDrawerItems();
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> mAdapterView, View mDrawerList, int position, long id) {
                switch (position) {
                    case 0:
                        callFragment();
                        lockNavigationBar();
                        Toast.makeText(MainActivity.this, "Add Student", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        logout();
                        break;
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                mDrawerToggle.syncState();
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        setupDrawer();
    }

    private void setupDrawer() {
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void addDrawerItems() {
        String[] osArray = {"Add Student", "logout"};
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case add:
                        StdData=new Student();
                        StdData.name = data.getStringExtra("name");
                        StdData.email = data.getStringExtra("email");
                        StdData.rollno = Long.parseLong(data.getStringExtra("rn"));
                        StdData.phone = Long.parseLong(data.getStringExtra("phone"));
                        new Downloader().execute(2);
                        addinlist();
                        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
                        break;
                    case edit:
                        StdData = (Student) data.getSerializableExtra("vi");
                        studentData.set(pc, StdData);
                        new Downloader().execute(3);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(this, "Edited", Toast.LENGTH_SHORT).show();
                        break;
                }
            } else {
                Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }*/
    @Override
    public void OnClick(int position) {
        switch (position) {
            case viewdata:
                viewdata();

                break;
            case editdata:
                editdata();
                break;
            case deletedata:
                deletedata();
                break;
        }
    }

//    protected void addinlist() {
//        studentData.add(StdData);
//        adapter.notifyDataSetChanged();
//    }

    protected void view() {
        DialogBox dialog = new DialogBox();
        dialog.setDialogListener(MainActivity.this);
        dialog.show(getFragmentManager(), "Dialog");
    }

    protected void viewdata() {
//        Intent in = new Intent(this, AddStudentForm.class);
//        in.putExtra("vi", studentData.get(pc));
//        in.putExtra("edit", false);
//        startActivityForResult(in, 10);
        callFragment();
        lockNavigationBar();
        new Downloader().execute(VIEW_SPECIFIC_ASYNC);
    }

    protected void editdata() {
//        Intent0 in1 = new Intent(this, AddStudentForm.class);
//        in1.pu0tExtra("vi", studentData.get(pc));
//        in1.putExtra("edit", true);
//        startActivityForResult(in1, 20);
//        Toast.makeText(this, "Edit...", Toast.LENGTH_SHORT).show();
        callFragment();
        lockNavigationBar();
        new Downloader().execute(VIEW_UPDATE_ASYNC);
    }

    void callFragment() {
        parent.setVisibility(View.GONE);
//        fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.add(R.id.fr1, addform, "start");
//        fragmentTransaction.commit();
fragmentManager = getFragmentManager();
        FragmentTransaction FT = fragmentManager.beginTransaction();
        addform = new AddStudentForm();
        FT.add(R.id.fr1, addform, "add_fragment");
        FT.commit();
    }

    protected void deletedata() {
        new Downloader().execute(DELETE_ASYNC);
        //  adapter.notifyDataSetChanged();
        Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
    }

    protected void logout() {
        preference.edit().clear().commit();
        Toast.makeText(MainActivity.this, "Logged Out", Toast.LENGTH_SHORT).show();
        Intent logout = new Intent(MainActivity.this, AdminBoard.class);
        startActivity(logout);
    }

    //    protected void addDetails() {
//        intentObj = new Intent(this, AddStudentForm.class);
//        startActivityForResult(intentObj, 1);
//    }
    void lockNavigationBar() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerLayout.closeDrawer(mDrawerList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    void unLockNavigationBar() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public void recieveData(Student obj, int request) {
        unLockNavigationBar();
        getFragmentManager().beginTransaction().remove(addform).commit();
        parent.setVisibility(View.VISIBLE);
        StdData = obj;
        switch (request) {
            case ADD_ASYNC:
                studentData.add(StdData);
                new Downloader().execute(ADD_ASYNC);
                Toast.makeText(this, StdData.getName() + "Details added", Toast.LENGTH_SHORT).show();
                break;

            case UPDATE_ASYNC:
                new Downloader().execute(UPDATE_ASYNC);
                Toast.makeText(this, StdData.getName() + "Updated", Toast.LENGTH_SHORT).show();
                break;

            case 0:
                break;
        }
    }

    public class Downloader extends AsyncTask<Integer, Integer, Integer> {
        DatabaseController dbc;
        Adapter adap;
        int choice;
        ProgressDialog progressDialog;
        Student stut;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();
            dbc = new DatabaseController(MainActivity.this);
            adap = (Adapter) listView.getAdapter();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            choice = params[0];
            switch (choice) {
                case VIEW_ALL_ASYN:
                    dbc.viewAll(studentData);
                    break;
                case ADD_ASYNC:
                    dbc.insertData(studentData.getLast());
                    break;
                case UPDATE_ASYNC:
                    dbc.update(StdData);
                    break;
                case DELETE_ASYNC:
                    dbc.delete(studentData.get(pc));
                    studentData.remove(pc);
                    break;
                case VIEW_SPECIFIC_ASYNC:
                    stut = studentData.get(pc);
                    break;
                case VIEW_UPDATE_ASYNC:
                    stut = studentData.get(pc);
                    break;

            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            AddStudentForm adb;
            switch (choice) {
                case VIEW_SPECIFIC_ASYNC:
             adb= (AddStudentForm) fragmentManager.findFragmentByTag("add_fragment");
                    adb.sendData(stut, VIEW_SPECIFIC_ASYNC);
                    break;
                case VIEW_UPDATE_ASYNC:
                    adb= (AddStudentForm) fragmentManager.findFragmentByTag("add_fragment");
                    adb.sendData(stut, VIEW_UPDATE_ASYNC);

                    break;
            }
            adap.notifyDataSetChanged();
        }
    }
}