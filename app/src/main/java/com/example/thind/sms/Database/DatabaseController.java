package com.example.thind.sms.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.thind.sms.Classes.Student;

import java.util.LinkedList;

/**
 * Created by Thind on 16-09-2015.
 */
public class DatabaseController implements DbConstants {
    DatabaseHandler dbHelper;
    SQLiteDatabase db;

    public DatabaseController(Context context) {
        dbHelper = new DatabaseHandler(context, "stu", null, 3);
    }


    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public long insertData(Student student) {
        open();
        ContentValues content = new ContentValues();
        content.put(Name, student.getName());
        content.put(Id, student.getRollno());
        content.put(Phone, student.getPhone());
        content.put(Email, student.getEmail());
        long result = db.insert(TABLE_NAME, null, content);
        close();
        return result;
        }

    public LinkedList<Student> viewAll(LinkedList<Student> students) {
        students.clear();
        String query = "SELECT * FROM " + TABLE_NAME + ";";
        open();
        Cursor cursor = db.rawQuery(query, null);
        Student student;
        if (cursor.moveToFirst()) {
            do {
                student = new Student();
                student.setName(cursor.getString(1));
                student.setRollno(Long.parseLong(cursor.getString(2)));
                student.setPhone(Long.parseLong(cursor.getString(3)));
                student.setEmail(cursor.getString(4));
                students.add(student);
            } while (cursor.moveToNext());
        }
        close();
        return students;
    }

    public void delete(Student student) {
        open();
        db.delete(dbHelper.TABLE_NAME, dbHelper.Id + " =?", new String[]{String.valueOf(student.getRollno())});
        close();
    }

    public int update(Student student) {
        open();
        ContentValues content = new ContentValues();
        content.put(Name, student.getName());
        content.put(Phone, student.getPhone());
        content.put(Email, student.getEmail());
        int id = db.update(dbHelper.TABLE_NAME, content, dbHelper.Id + " =?", new String[]{String.valueOf(student.getRollno())});
        close();
        return id;
    }
}
