package com.example.thind.sms.Comparator;

import com.example.thind.sms.Classes.Student;

import java.util.Comparator;

/**
 * Created by Thind on 10-09-2015.
 */
public class ByName implements Comparator<Student>{
    @Override
    public int compare(Student lhs, Student rhs) {
        String cmp1 =lhs.getName();
        String cmp2 =rhs.getName();
        int result=cmp1.compareToIgnoreCase(cmp2);
        if(result>0)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
}
