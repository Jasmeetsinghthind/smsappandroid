package com.example.thind.sms.Classes;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thind.sms.R;

public class AdminBoard extends AppCompatActivity implements View.OnClickListener {
    Button login, cancel;
    String name1;
    String pwd1;
    public static final String name2 = "thind";
    public static final String pwd2 = "12345";
    EditText name, password;
    SharedPreferences preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_board);
        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(this);
        cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        name = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.password);
        preference = PreferenceManager.getDefaultSharedPreferences(this);
        String FlagValue = preference.getString("flag value", "");
        if (FlagValue.equals("true")) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        name1 = preference.getString("key1", "");
        pwd1 = preference.getString("key2", "");
        name.setText(name1);
        password.setText(pwd1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_admin_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:

                login();
                break;
            case R.id.cancel:
                exit();
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    protected void exit() {
        Toast.makeText(this, "Good Bye...", Toast.LENGTH_SHORT).show();
        finishAffinity();
    }

    protected void login() {
        name1 = name.getText().toString();
        pwd1 = password.getText().toString();
        if (name1.equals(name2) && pwd1.equals(pwd2)) {
            SharedPreferences.Editor editor = preference.edit();
            editor.putString("flag value", "true");
            editor.putString("key1", name1);
            editor.putString("key2", pwd1);
            editor.commit();
            Intent AdmBoard = new Intent(this, MainActivity.class);
            startActivity(AdmBoard);
            Toast.makeText(this, "Logged In", Toast.LENGTH_SHORT).show();
            finish();
        }
        else if (name1.equals("") || pwd1.equals("")) {
            if (name1.equals("") && pwd1.equals("")) {
                Toast.makeText(this, "Enter username and password", Toast.LENGTH_SHORT).show();
            } else if (name1.equals("")) {
                Toast.makeText(this, "Enter username", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            if (!name1.equals(name2) || !pwd1 .equals(pwd2)) {
                if (!name1.equals(name2) && !pwd1 .equals(pwd2)) {
                    Toast.makeText(this, "Enter valid Username and Password", Toast.LENGTH_SHORT).show();
                } else if (!name1.equals(name2)) {
                    Toast.makeText(this, "Enter valid Username", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Enter valid Password", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}