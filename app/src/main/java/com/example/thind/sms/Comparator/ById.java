package com.example.thind.sms.Comparator;

import java.util.Comparator;
import com.example.thind.sms.Classes.Student;
/**
 * Created by Thind on 10-09-2015.
 */
public class ById implements Comparator<Student> {
    @Override
    public int compare(Student lhs, Student rhs) {
        long cmp1=lhs.getRollno();
        long cmp2 =rhs.getRollno();
        if(cmp1>cmp2)
        {
            return 1;
        }
        else
        {
            return -1;
        }

    }
}
