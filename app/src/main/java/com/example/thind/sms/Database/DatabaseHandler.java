package com.example.thind.sms.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Thind on 16-09-2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper implements DbConstants {


    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_Version);
    }


        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(TABLE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL("DROP TABLE IF EXISTS mytable");
                onCreate(db);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }



//    public Cursor returnData() {
//        return db.query(TABLE_NAME, new String[]{Name, Id, Phone, Email}, null, null, null, null, null);
//    }

