package com.example.thind.sms.Classes;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thind.sms.R;
import com.example.thind.sms.util.Conventions;
import com.example.thind.sms.util.DataRecieveInterface;
import com.example.thind.sms.util.DataSendInterface;

public class AddStudentForm extends Fragment implements View.OnClickListener, Conventions, DataSendInterface {
    protected EditText enterName = null;
    protected EditText enterEmail = null;
    protected EditText enterPhone = null;
    protected EditText enterRollNo = null;
    Student student;
    boolean isEditable;
    InputMethodManager imm;
    DataRecieveInterface recieveDataObj;
    Button save;
    Button cancel;

    //  DatabaseController db;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_add_student_form, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // setContentView(R.layout.activity_add_student_form);
        enterName = (EditText) getActivity().findViewById(R.id.enterName);
        enterEmail = (EditText) getActivity().findViewById(R.id.enterEmail);
        enterPhone = (EditText) getActivity().findViewById(R.id.enterPhone);
        enterRollNo = (EditText) getActivity().findViewById(R.id.enterRollNo);
        cancel = (Button) getActivity().findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        save = (Button) getActivity().findViewById(R.id.save);
        save.setOnClickListener(this);
        recieveDataObj = (DataRecieveInterface) getActivity();
        /*if (getActivity().getIntent().hasExtra("vi")) {
            student = (Student) getActivity().getIntent().getSerializableExtra("vi");
            isEditable = getActivity().getIntent().getBooleanExtra("edit", true);
            //  isedit =getIntent().getBooleanExtra("edit",false);
        }
        if (student != null) {
            enterName.setText(student.getName());
            enterEmail.setText(student.getEmail());
            enterPhone.setText(String.valueOf(student.getPhone()));
            enterRollNo.setText(String.valueOf(student.getRollno()));
            enterName.setEnabled(isEditable);
            enterEmail.setEnabled(isEditable);
            enterPhone.setEnabled(isEditable);
            enterRollNo.setEnabled(false);
            if (!isEditable) {
                save.setVisibility(View.INVISIBLE);
            } else {
                save.setVisibility(View.VISIBLE);
            }
        }*/
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.save:

                String stuName = enterName.getText().toString();
                String stuRoll = enterRollNo.getText().toString();
                String stuPhone = enterPhone.getText().toString();
                String stuEmail = enterEmail.getText().toString();
                if ((!stuName.isEmpty()) && (!stuRoll.isEmpty()) && (!stuPhone.isEmpty()) && (!stuEmail.isEmpty())) {
                    // Intent intent = new Intent();
                    if (isEditable) {
                        student.setName(stuName);
                        student.setRollno(Long.parseLong(stuRoll));
                        student.setPhone(Long.parseLong(stuPhone));
                        student.setEmail(stuEmail);
                        clearEdittext();
                        closeKeyboard(v);
                        recieveDataObj.recieveData(student, UPDATE_ASYNC);
                        //       intent.putExtra("vi", student);
                    } else {
                        student = new Student();
                        student.setName(stuName);
                        student.setRollno(Long.parseLong(stuRoll));
                        student.setPhone(Long.parseLong(stuPhone));
                        student.setEmail(stuEmail);
                        clearEdittext();
                        closeKeyboard(v);
                        recieveDataObj.recieveData(student, ADD_ASYNC);
                    }
//            intent.putExtra("name",stuName);
//            intent.putExtra("rn",stuRoll);
//            intent.putExtra("phone",stuPhone);
//            intent.putExtra("email",stuEmail);
//          getActivity().setResult(getActivity().RESULT_OK, intent);
//                    getActivity().finish();
                } else if ((stuName.isEmpty())) {
                    Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
                } else if ((stuRoll.isEmpty())) {
                    Toast.makeText(getActivity(), "Enter Roll Number", Toast.LENGTH_SHORT).show();
                } else if ((stuPhone.isEmpty())) {
                    Toast.makeText(getActivity(), "Enter Phone number", Toast.LENGTH_SHORT).show();
                } else if (stuEmail.isEmpty()) {
                    Toast.makeText(getActivity(), "Enter Email Address", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.cancel:
                clearEdittext();
                closeKeyboard(getView());
//                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                recieveDataObj.recieveData(null, 0);
                break;
//    getActivity().setResult(getActivity().RESULT_CANCELED);
//    getActivity().finish();
        }
    }

    @Override
    public void sendData(Student obj, int request) {
        student=obj;
        if (obj != null) {
            // rollno=student.getRollNo();
            enterName.setText(student.getName());
            enterRollNo.setText(String.valueOf(student.getRollno()));
            enterPhone.setText(String.valueOf(student.getPhone()));
            enterEmail.setText(student.getEmail());
        }
        switch (request) {
            case VIEW_SPECIFIC_ASYNC:
                isEditable = false;
                enterName.setEnabled(isEditable);
                enterRollNo.setEnabled(isEditable);
                enterPhone.setEnabled(isEditable);
                enterEmail.setEnabled(isEditable);
                //  enterDataTextView.setText(DISPLAY_DATA_TEXT + student.getRollNo());
                save.setVisibility(View.INVISIBLE);
                enterName.setTextColor(Color.rgb(0, 0, 0));
                enterRollNo.setTextColor(Color.rgb(0, 0, 0));
                enterPhone.setTextColor(Color.rgb(0, 0, 0));
                enterEmail.setTextColor(Color.rgb(0, 0, 0));
                break;

            case VIEW_UPDATE_ASYNC:
                isEditable = true;
                enterName.setEnabled(isEditable);
                enterRollNo.setEnabled(false);
                enterPhone.setEnabled(isEditable);
                enterEmail.setEnabled(isEditable);
                /*Move cursor to the end of text*/
                enterName.setSelection(enterName.getText().length());
                //enterDataTextView.setText(EDIT_DATA_TEXT + student.getRollNo());
                save.setVisibility(View.VISIBLE);
                openKeyboard();
                break;
        }
    }

    void clearEdittext() {
        enterName.setText("");
        enterRollNo.setText("");
        enterPhone.setText("");
        enterEmail.setText("");
    }

    // Show soft keyboard for the user to enter the value.
    void openKeyboard() {
        enterName.requestFocus();
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(enterName, InputMethodManager.SHOW_FORCED);
    }

    //Close keyboard when activity is called
    void closeKeyboard(View v) {
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}