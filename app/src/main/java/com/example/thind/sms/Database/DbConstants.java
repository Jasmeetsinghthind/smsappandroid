package com.example.thind.sms.Database;

/**
 * Created by Thind on 16-09-2015.
 */
public interface DbConstants {
    String Name = "name";
    String Id = "id";
    String Phone = "phone";
    String Email = "email";
    String TABLE_NAME = "myTable";
    String DATA_BASE_NAME = "myDatabase";
    int DATA_BASE_Version = 1;
    String Uid ="_id";
    String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + "( " + Uid + " INTEGER PRIMARY KEY , " + Name + " VARCHAR(255) , " + Id + " NUMBER , " + Phone + " NUMBER , " + Email + " VARCHAR(255) );";
}
