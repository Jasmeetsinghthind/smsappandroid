package com.example.thind.sms.util;

/**
 * Created by Thind on 14-09-2015.
 */
public interface Conventions {
  int  add=1,view=10,edit=20,viewdata=1,editdata=2,deletedata=3,
          ADD_ASYNC=1,UPDATE_ASYNC=2,
          VIEW_SPECIFIC_ASYNC=3
          ,VIEW_ALL_ASYN=4,VIEW_UPDATE_ASYNC=6,
  DELETE_ASYNC=5;
}
